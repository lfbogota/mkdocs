# Automatiser des tâches avec Cron

Parfois, vous ne souhaitez pas lancer manuellement un script que vous avez écrit. Vous pouvez avoir besoin que le script soit exécuté une fois par heure, ou peut-être une fois toutes les trente secondes, ou encore à chaque fois que votre ordinateur démarre. Sur les systèmes *nix, c'est une tâche assez facile, car vous pouvez utiliser un programme appelé Cron. Cron exécutera toute commande que vous lui demanderez d'exécuter, à chaque fois que vous l'aurez programmé. Il référencera ce que l'on appelle la table cron, qui est normalement abrégée en crontab.

## Modification de la crontab

Pour ouvrir la crontab, vous devez d'abord ouvrir une fenêtre de terminal. Ensuite, vous pouvez taper

``` console
crontab -e
```

- Le `-e` de cette commande est l'abréviation de `edit`. Si c'est la première fois que vous ouvrez votre crontab, il vous sera demandé quel éditeur de texte vous souhaitez utiliser.

- choix de l'éditeur

L'éditeur le plus simple à utiliser est nano, tapez 2 pour le choisir et appuyez sur <kbd>Entrée</kbd>.

img

- nano est un simple éditeur de texte en ligne de commande. Si vous souhaitez en savoir plus sur l'utilisation de nano, vous pouvez consulter cette ressource.

Syntaxe de la crontab
La crontab contient toutes les informations de base dont vous avez besoin pour commencer. Chaque ligne qui commence par un # est un commentaire, et est donc ignorée par l'ordinateur. Au bas de la crontab, vous devriez voir une ligne qui ressemble à ceci :

``` console
# m h dom mon dow commande
```

- `m` est l'abréviation de minute
- `h` est l'abréviation de hour
- `dom` est l'abréviation de day of the month (jour du mois)
- `mon` est l'abréviation de mois
- `dow` est l'abréviation de "day of the week" (jour de la semaine)
- `command` est la commande bash que vous souhaitez exécuter.

## Création d'une nouvelle tâche Cron

Pour créer une tâche Cron, vous devez décider dans quelles circonstances vous souhaitez qu'elle soit exécutée. Par exemple, si vous voulez exécuter un script Python à la 30ème minute de chaque heure, vous écrirez ce qui suit :

``` console
30 * * * * python3 /home/pi/my_cool_script.py
```

Si vous vouliez qu'il s'exécute toutes les 30 minutes, vous utiliseriez :

``` console
*/30 * * * * python3 /home/pi/my_cool_script.py
```

La valeur 30 indique au script de s'exécuter toutes les 30 minutes. Les astérisques indiquent que le script doit être exécuté pour toutes les valeurs légales des autres champs.

Voici quelques autres exemples.
|Ce qui va se passer...   |  syntaxe crontab |
|---|---|
|   |   |

Exécuter un script à 11:59 tous les mardis 59 11 ** 2 python3 /home/pi/my_script.py
Exécuter un script une fois par semaine le lundi 0 0 * * 1 python3 /home/pi/my_script.py
Exécuter un script à 12:00 le 1er janvier et le 1er juin 0 12 1 1,6 * python3 /home/pi/my_script.py

## Exécution au démarrage

Une fonctionnalité incroyablement utile de Cron est sa capacité à exécuter une commande au démarrage de l'ordinateur. Pour ce faire, vous utilisez la syntaxe @reboot. Par exemple

``` console
@reboot python3 /home/pi/my_cool_script.py
```

## Modifiez et enregistrez le fichier

Vous pouvez ajouter votre tâche cron au bas de la crontab. Ensuite, enregistrez et quittez nano en appuyant sur <kbd>Ctrl</kbd> + <kbd>x</kbd>, puis en tapant `y` lorsque vous êtes invité à enregistrer.
