# Sommaire

1. [Commander le Raspberry Pi à distance](cron.md)
1. [Utiliser la ligne de commande](terminal.md)
1. [Programmer des taches avec cron](cron.md)
1. Envoyer un email avec Python
1. [Publier sur le réseau social Mastodon avec un bot](mastodon.md)
1. Serveur web avec Flask
1. Manipuler des données au format `.csv`
1. Interagir avec une base de données
