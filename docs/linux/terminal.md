# Ligne de commande

## Ouvrir une fenetre de terminal

## Changer le mot de passe

Une des premières choses à faire sur le raspberry est de changer le mot de passe par défaut par un mot de passe de votre choix avec la commande `passwd` :

``` console
passwd
```

Vous devez choisir un mot de passe suffisamment complexe (8 caractères minimum et ne contenant pas de mot du dictionnaire).

## Prompt et répertoire courant

On appelle _prompt_ les informations affichées au début de chaque ligne :

``` console
utilisateur@machine:~$
```

Dans le cas où l'utilisateur est `pi`et le nom de la machine est `raspberrypi` alors le prompt est :

``` console
pi@raspberrypi:~$
```

Le caractère `~` est un alias pour désigner votre répertoire personnel. Lors de l'ouverture d'un terminal le répertoire courant est toujours le répertoire personnel de l'utilisateur.

Pour afficher le répertoire courant vous pouver utiliser la commande `pwd` (_print working directory_) :

``` console
$ pwd
/home/pi
```

Le répertoire `/home` contient les répertoires personnels de tous les utilisateurs de ce raspberry.

## Manipuler les fichiers

### Lister les fichiers

On peut lister le contenu du répertoire courant avec la commande suivante :

``` console
ls
```

Si rien ne s’affiche c'est que votre répertoire est vide.

On peut lister le contenu d’un répertoire qui n’est pas le répertoire courant en le spécifiant en option.

``` console
$ ls /home
alvaro juan maria pi prof sara
```

### Créer un fichier

La commande `touch` permet de créer un fichier :

``` console
touch notes.txt
```

!!! note "Pour s'entrainer"
    - Créer les fichiers `readme.txt`, `todo.txt`, `temp.txt` et `programme.py` à l’aide de la commande `touch`.
    - Vérifier que les fichiers ont bien été créés en exécutant la commande `ls`.

!!!tip "Remarque"
    On peut spécifier plusieurs paramètres à la commande `touch` afin de créer plusieurs fichiers en une fois.

    ``` console
    touch readme.txt todo.txt temp.txt programme.py
    ```

### Editer un fichier

L’éditeur de texte `nano` est installé sur la majorité des distributions linux et va nous permettre de modifier les fichiers précédemment créés.

La commande suivante permet d’ouvrir le fichier `readme.txt` afin de l’éditer.

``` console
nano readme.txt
```

!!! note "Pour s'entrainer"
    - Ajouter une ligne de texte dans le fichier `readme.txt`. Sauvegarder vos modifications avec la touche <kbd>Ctrl</kbd> + <kbd>O</kbd> puis quitter l'édieur nano avec  <kbd>Ctrl</kbd> + <kbd>X</kbd>.

!!! tip "Remarque"
    Une fois que vous avez entrer les premières lettres d’un fichier ou d’un dossier, vous pouvez appuyer sur la touche <kbd>TAB</kbd> afin d’activer la complétion automatique et de ne pas avoir à taper le nom entier du fichier ou du dossier.

### Afficher le contenu d’un fichier

La commande `cat` permet d’afficher le contenu d’un fichier depuis la ligne de commande sans avoir à ouvrir un éditeur.

``` console
cat todo.txt
```

## Répertoires

### Créer un répertoire

La commande `mkdir` permet de créer un nouveau répertoire.

``` console
mkdir images
```

!!! note "Pour s'entrainer"
    - Créer un répertoire `python`, un répertoire `documents`  et un répertoire `videos` dans votre répertoire personnel.

!!!tip "Remarque"
    On peut spécifier plusieurs paramètres à la commande `mkdir` afin de créer plusieurs répertoires en une fois.

    ``` console
    mkdir dossier1 dossier2 dossier3
    ```

### Se déplacer dans l'arborescence

Pour se déplacer à l’intérieur d'un répertoire on utilise la commande `cd` (_change directory_).

``` console
cd images
```

Le prompt indique maintenant que vous vous trouvez dans le répertoire images.

Pour remonter d’un niveau on utilise la commande suivante :

``` console
cd ..
```

Et pour revenir à notre dossier utilisateur :

``` console
cd ~
```

### Contenu d'un répertoire

Pour afficher plus d’informations sur le contenu d’un répertoire, on peut ajouter l’option `-l` à la commande `ls` :

``` console
ls -l
```

!!! note "Pour s'entrainer"
    - Exécuter la commande `ls -l` à la racine de votre répertoire utilisateur
    - La taille de chaque fichier est maintenant affichée, pouvez-vous identifier la colonne contenant cette information ?
    - Exécuter la commande `ls -l --human-readable` pour lister les fichiers avec la taille exprimée dans un format "lisible par un humain" (ko, Mo , Go).

!!! tip "Remarque"
    Le premier caractère de chaque ligne indique le type de l'élément :

    - `-` : fichier
    - `d` : répertoire
    - `l` : lien symbolique
    - `s` : socket

    Les 9 caractères suivant indiquent les permissions qui lui sont rattachées.

!!! note "Pour s'entrainer"
    - Exécuter la commande `ls -l --human-readable` pour lister les fichiers avec la taille exprimée dans un format "lisible par un humain" (ko, Mo , Go).

!!! tip "Remarque"
    Les options d'une commande existent dans une forme longue (`--human-readable`) ou dans une forme courte `-h`. On peut combiner les options sous leurs forme courte en les plaçant les unes à la suite des autres. Par exemple la commande `ls -lh` est équivalente à la commande `ls -l --human-readable`.

### Fichiers cachés

Les fichiers dont le nom commence par un point sont considérés comme cachés et ne sont pas affichés par défaut. Ils sont souvent utilisés pour stocker les options de configuration d'un programme.

!!! note "Pour s'entrainer"
    - Créer un fichier `.config` et vérifier qu’il n’est pas visible avec la commande `ls`
    - Ouvrir le manuel de la commande `ls` en tapant `man ls` et chercher quelle est l’option à ajouter pour que les fichiers cachés soit affichés.
    - Quitter le manuel en appuyant sur `q` et entrer la commande permettant de lister tous les fichiers du répertoire courant.

!!! tip "Remarque"
    Lors de l’affichage des fichiers cachés vous devriez voir apparaitre une ligne avec le point seul (`.`) et une ligne avec des doubles points (`..`). Ce sont des _alias_ pour le répertoire courant et le répertoire parent.

## Exécuter un script

Créer un fichier `script.py` puis éditer le avec nano pour ajouter ces quelques instructions python :

``` console
for i in range(10):
    print(i)
```

Attention nano est un éditeur basique et c’est à vous de réaliser l’indentation en ajoutant 4 espaces au début de la seconde ligne.

Enregistrer le fichier puis exécuter le à partir du terminal :

``` console
python3 script.py
```

## Joker

Le caractère astérisque `*` permet d’indiquer la présence d’un ou plusieurs caractères inconnus.  
On peut lister tous les fichiers dont l’extension est `.txt` de la manière suivante :

``` console
ls *.txt
```

!!! note "Pour s'entrainer"
    - Lister tous les fichiers de votre répertoire courant au format `.py`.

## Historique

Les touches `haut` et `bas` permettent de faire défiler les commandes précédemment tapées.  
On peut aussi accéder à l’ensemble de l’historique des commandes avec la commande `history`:

``` console
$ history
 1 touch readme.txt
 2 touch programme.py
 3 ls
 4 ls -al
 5 ls -hm
 6 nano readme.txt
 7 cat readme.txt
 8 pwd
 9 ls *.py
10 ls /home
11 ls /bin
12 history
```

On peut ensuite répéter une commande en utilisant son numéro précédé de `!`.

``` console
!6
```

## Copier, renommer et supprimer des fichiers

La commande `mv` permet de déplacer un fichier. Par exemple pour déplacer le fichier `script.py` dans le répertoire `python` créé auparavant on utilise la commande suivante :

``` console
mv programme.py python
```

Dans ce cas le premier paramètre est le fichier à déplacer et le deuxième paramètre est le répertoire de destination.

La commande `mv` permet aussi de renommer le fichier si le deuxième paramètre est le nouveau nom du fichier.

``` console
mv programme.py script.py
```

On peut combiner les instructions pour déplacer et renommer le fichier en une fois :

``` console
mv script.py /python/programme.py
```

!!! note "Pour s'entrainer"
    - Déplacer les fichiers `script.py` et `programme.py` dans le répertoire `python`
    - Déplacer les fichiers `readme.txt` et `todo.txt` dans le répertoire `documents`
    - Renommer le fichier `temp.txt` en `asupprimer.txt`

!!! tip "Remarque"
    - On peut utiliser le caractère `*` pour sélectionner plusieurs fichiers et les déplacer en une seule commande.
    Par exemple la commande suivante déplace tous les fichiers avec une extension `.py` dans le répertoire `/python` :

    ``` console
    mv *.py /python
    ```

La commande `rm` permet de supprimer le fichier passé en paramètre et la commande `rmdir` permet de supprimer un répertoire.  

!!! note "Pour s'entrainer"
    - Effacer le fichier `asupprimer.txt`.
