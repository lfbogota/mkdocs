# Interagir avec une base de données

## Installation de __DB Browser for SQLite__

``` console
sudo apt-get update
sudo apt-get install sqlitebrowser
```

Créer un dossier `station`.

## Création de la base de données

### Avec DB Browser

- New database
- `meteo.db`
- Table pour les températures :
  - Nom : `temperatures`
  - Add Field, date TEXT
  - Add Field temp REAL

test de l'insertion

```python
import sqlite3
import datetime

con = sqlite3.connect("meteo.db")
cur = con.cursor()

now = datetime.datetime.now()

cur.execute("INSERT INTO temperatures(date, temp) VALUES (?, ?)", (now.isoformat(), 21))

con.commit()
con.close()
```

vérifier que les données sont mises à jour dans db browser

## Vider une table

``` python
con = sqlite3.connect("meteo.db")
cur = con.cursor()

cur.execute("DELETE FROM temperatures")

con.commit()
con.close()
```

## Boucle

``` python
import sqlite3
import datetime
import random

for i in range(10):
    con = sqlite3.connect("tt.db")
    cur = con.cursor()
    now = datetime.datetime.now()
    temp = ... #todo
    cur.execute("INSERT INTO temperatures(date, temp) VALUES (?, ?)", (now.isoformat(), temp))
    con.commit()
    con.close()
    sleep(60)
```

Lecture

``` python
con = sqlite3.connect("meteo.db")
cur = con.cursor()

for row in cur.execute("select * from temperatures"):
    print(row)

con.close()
```

```python
con = sqlite3.connect("meteo.db")
cur = con.cursor()

for row in cur.execute("select * from temperatures"):
    date = datetime.datetime.fromisoformat(row[0])
    temp = row[1]
    print(f'{date.hour:02d}:{date.minute:02d} {temp:>6}°')

con.close()
```

``` console
22:07   22.0°
22:08   22.0°
22:09   22.0°
22:10   22.0°
22:11   22.0°
22:12   22.0°
22:13   22.0°
22:14   22.0°
22:15   22.0°
```

## Interface web

<https://blog.miguelgrinberg.com/post/run-your-flask-regularly-scheduled-jobs-with-cron/page/2>

```console
FLASK_APP="web.py"
```

``` python
@app.cli.command()
def measure():
    ...
```

```console hl_lines="20"
$ nano meteoflask.py 
pi@rasperry:~/station$ flask --help
Usage: flask [OPTIONS] COMMAND [ARGS]...

  A general utility script for Flask applications.

  Provides commands from Flask, extensions, and the application. Loads the
  application defined in the FLASK_APP environment variable, or from a wsgi.py
  file. Setting the FLASK_ENV environment variable to 'development' will
  enable debug mode.

    $ export FLASK_APP=hello.py
    $ export FLASK_ENV=development
    $ flask run

Options:
  --version  Show the flask version
  --help     Show this message and exit.

Commands:
  measure  mesure la température
  routes   Show the routes for the app.
  run      Run a development server.
  shell    Run a shell in the app context.
```
