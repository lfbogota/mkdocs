# Prise en main du Raspberry Pi

## Connexion des périphériques

Le Raspberry Pi est un nano-ordinateur monocarte à processeur ARM conçu par des professeurs du département informatique de l’université de Cambridge dans le cadre de la fondation Raspberry Pi. Cet ordinateur, de la taille d’une carte de crédit, permet l’exécution de plusieurs variantes du système d’exploitation libre GNU/Linux.

Il est fourni nu, c’est-à-dire avec seulement la carte mère, sans boı̂tier, alimentation, clavier, souris ni écran, dans l’objectif de diminuer les coûts et de permettre l’utilisation de matériel de récupération. Le Raspberry Pi est utilisé par des makers du monde entier car son prix le rend très attirant (à partir de 35$). Le Raspberry Pi est un nano-ordinateur, contrairement à Arduino qui est une carte électronique.

!!!note "A faire"
    - Connecter la souris et le clavier aux ports USB du Raspberry Pi.
    - Relier l’écran au Raspberry Pi à l’aide du cable HDMI.
    - Brancher ensuite le Raspberry Pi sur l’alimentation et vérifier que le système démarre.
    - Se connecter au WiFi du lycée avec les identifiants habituels.
