# Stop motion

## Introduction

Réalise ta propre vidéo d'animation en stop motion en utilisant un Raspberry Pi, Python et un module caméra pour prendre des photos, contrôlées par un bouton poussoir connecté aux broches GPIO du Pi.

Vous pouvez utiliser des LEGO pour animer une tour en construction, des personnages jouant une scène, ou tout ce que vous pouvez imaginer !

<!-- ajouter un gig -->

!!! tip Ce que tu vas apprendre

    En créant une machine à bouton poussoir avec votre Raspberry Pi, vous allez apprendre à :

    - configurer et utiliser le module caméra du Raspberry Pi.
    - utiliser la bibliothèque Python `picamera` pour prendre des photos
    - connecter un bouton aux broches GPIO d'un Raspberry Pi
    - contrôler la caméra à l'aide d'un bouton en utilisant GPIO Zéro
    - générer une vidéo à partir de la ligne de commande en utilisant `avconv`

!!!abstract "Matériel"
    Pour réaliser ce projet vous allez avoir besoin de :

    - Module caméra Raspberry Pi
    - 1 plaque de connection (_breadboard_)
    - 2 fils de liaison mâle-femelle
    - 1 bouton tactile
    - logiciel `ffmpeg` devrait être préinstallé sur la dernière version du système d'exploitation de Raspberry Pi. S'il n'est pas installé, ouvrez un terminal et tapez :
        ``` console
        sudo apt update
        sudo apt upgrade
        sudo apt install ffmpeg
        ```

## Connecter la caméra

Avant de démarrer votre Pi, vous devez connecter la caméra.

![Module caméra](img/stopmotion-camera.jpg)

!!!note "A faire"
    - Localisez le port de la caméra à côté du port Ethernet. Soulevez la languette sur le dessus.

    - Placez la bande dans le connecteur, le côté bleu faisant face au port Ethernet. Poussez la languette vers le bas tout en maintenant la bande en place.

    - Mettez le Pi sous tension pour le faire démarrer.

## Tester la caméra

!!!note "A faire"
    - Ouvrez une fenêtre de terminal à partir du menu de l'application. Entrez la commande suivante :

        ``` console
        raspistill -k
        ```

    - Vous devriez voir un aperçu apparaître à l'écran. Il n'est pas important que l'image soit à l'envers ; vous pourrez configurer cela plus tard. Appuyez sur <kbd>Ctrl + C</kbd> pour quitter l'aperçu.

    - Pour enregistrer une image, vous pouvez utiliser la commande suivante :

        ``` console
        raspistill -o image1.jpg
        ```
        L'aperçu va apparaitre pendant 5 secondes et la photo sera prise ensuite.

    - Exécutez la commande `ls` pour lister les fichiers dans votre répertoire personnel ; vous devriez voir `image1.jpg` dans la liste.

    - Cliquez sur l'icône du gestionnaire de fichiers dans la barre des tâches et vous devriez voir quelques dossiers et fichiers. Double-cliquez sur `image1.jpg` pour le visualiser.

## Prendre une photo avec Python

- Ouvrez Python 3 (IDLE) à partir du menu principal :

    ![éditeur IDLE](img/pi-idle.png)

- Sélectionnez <kbd>Fichier</kbd> > <kbd>Nouvelle fenêtre</kbd> dans le menu pour ouvrir l'éditeur de fichiers Python.

- Saisissez soigneusement le code suivant dans la nouvelle fenêtre :

    ``` python
    from picamera import PiCamera
    from time import sleep

    camera = PiCamera()

    camera.start_preview()
    sleep(3)
    camera.capture('/home/pi/Desktop/image.jpg')
    camera.stop_preview()
    ```

- Sélectionnez <kbd>Fichier</kbd> > <kbd>Enregistrer</kbd> dans le menu (ou appuyez sur <kbd>Ctrl + S</kbd>) et enregistrez sous le nom de `animation.py`.

- Appuyez sur <kbd>F5</kbd> pour exécuter votre programme.

    Vous devriez voir le fichier `image.jpg` enregistré sur votre bureau. Double-cliquez sur l'icône pour ouvrir l'image.

- Si l'image est à l'envers, vous pouvez soit repositionner votre appareil photo à l'aide d'un support, soit le laisser tel quel et demander à Python de retourner l'image. Pour ce faire, ajoutez la ligne suivante :

    ``` python hl_lines="6"
    from picamera import PiCamera
    from time import sleep

    camera = PiCamera()

    camera.rotation = 180
    camera.start_preview()
    sleep(3)
    camera.capture('/home/pi/Desktop/image.jpg')
    camera.stop_preview()
    ```

  - Exécutez à nouveau le programme et il écrasera `image.jpg` avec une nouvelle image dans la bonne orientation. N'oubliez pas de conserver ces lignes dans votre code pendant que vous le modifiez au cours des prochaines étapes.

## Connecter un bouton poussoir

À l'aide de votre _breadboard_ et de fils de liaison, connectez le Pi au bouton comme indiqué dans le schéma ci-dessous :

![Connecter le bouton pourssoir](https://projects-static.raspberrypi.org/projects/push-button-stop-motion/a7d941516a23f7fd4c5419d4dd4b7a876be10d38/en/images/picamera-gpio-setup.png)

Importez `Button` du module `gpiozero` en haut du code et créez un `Button` connecté à la broche 17. Changez ensuite la ligne avec l'instruction `sleep` pour utiliser `button.wait_for_press` comme ci-dessous :

``` python hl_lines="3 5 9"
from picamera import PiCamera
from time import sleep
from gpiozero import Button

button = Button(17)
camera = PiCamera()

camera.start_preview()
button.wait_for_press()
camera.capture('/home/pi/image.jpg')
camera.stop_preview()
```

!!!note "A faire"
    - Enregistrez et exécutez votre programme.
    - Une fois l'aperçu lancé, appuyez sur le bouton connecté à votre Pi pour capturer une image.
    - Revenez à la fenêtre du gestionnaire de fichiers et vous devriez voir le fichier `image.jpg`. Encore une fois, double-cliquez pour le visualiser.

## Prendre un selfie

Si vous voulez prendre une photo de vous-même avec la carte caméra, vous allez devoir ajouter un délai pour vous permettre de vous mettre en position. Vous pouvez le faire en rajoutant une instruction `sleep` dans votre programme.

!!!note "A faire"
    - Ajoutez une ligne à votre code pour indiquer au programme de se mettre en pause brièvement avant de capturer une image, comme ci-dessous :

        ``` python hl_lines="3"
        camera.start_preview()
        button.wait_for_press()
        sleep(3)
        camera.capture('/home/pi/Desktop/image.jpg')
        camera.stop_preview()
        ```

    - Enregistrez et exécutez votre programme.

    - Appuyez sur le bouton et essayez de prendre un selfie. Veillez à ce que la caméra reste immobile ! 

    - Encore une fois, n'hésitez pas à vérifier la présence de l'image dans le gestionnaire de fichiers. Vous pouvez exécuter le programme à nouveau pour prendre un nouveau selfie.

## Animation en stop motion

Maintenant que vous avez réussi à prendre des photos individuelles avec votre appareil, nous allons combiner une série de photos pour réaliser une animation en stop motion.

!!!warning "Important"
    Vous devez créer un nouveau dossier `animation` pour enregistrer vos photos. Dans la fenêtre du terminal, entrez la commande :

    ``` console
    mkdir animation
    ```

Modifiez votre code pour ajouter une boucle `while` afin de continuer à prendre des photos à chaque fois que vous appuyez sur le bouton :

``` python
camera.start_preview()
frame = 1
while True:
    try:
        button.wait_for_press()
        camera.capture('/home/pi/animation/frame%03d.jpg' % frame)
        frame += 1
    except KeyboardInterrupt:
        camera.stop_preview()
        break
```

L'instruction `while True` démarre une boucle qui va se répeter indéfiniment et notre programme ne va jamais s'arrêter. Pour pouvoir sortir de notre programme de façon élégante, on utilise l'instruction `try` suivi de `except` pour pouvoir gérer une circonstance exceptionnelle : si vous forcez le programme à s'arrêter avec <kbd>Ctrl + C</kbd>, il fermera l'aperçu de la caméra et quittera la boucle.

!!! tip "Remarque"
    `frame%03d` signifie que le fichier sera enregistré sous le nom "frame" suivi d'un nombre à 3 chiffres avec des zéros en tête - 001, 002, 003, etc. Cela permet de les classer facilement dans l'ordre correct pour la vidéo.

!!!note "A faire"
    - Configurez maintenant votre scène d'animation pour être prêt à commencer votre animation image par image.

    - Appuyez sur le bouton pour capturer la première image, puis modifier votre scène et appuyez à nouveau sur le bouton pour capturer l'image suivante.

    - Une fois que toutes les images ont été capturées, appuyez sur <kbd>Ctrl + C</kbd> pour terminer le programme.

    - Ouvrez le dossier `animation` dans le gestionnaire de fichiers pour voir votre collection d'images fixes.

## Générer la vidéo

!!!note "A faire"
    - Pour générer la vidéo, commencez par revenir à la fenêtre du terminal.

    - Exécutez la commande de rendu vidéo :

        ``` console
        ffmpeg -r 10 -i animation/frame%03d.jpg -qscale 2 animation.mp4
        ```

    - Notez que vous utilisez à nouveau `%03d` - il s'agit d'un format commun que Python et ffmpeg comprennent, ce qui permet de transmettre les photos à la vidéo dans le bon ordre.

    - Lisez votre vidéo en lançant VLC directement depuis le terminal :

        ``` console
        vlc animation.mp4
        ```

    - Vous pouvez ajuster la fréquence d'images en modifiant la commande de rendu. Essayez de remplacer l'option `-r 10` (10 images par seconde) par un autre nombre.

    - Vous pouvez également modifier le nom de fichier de la vidéo rendue pour l'empêcher d'écraser votre première tentative. Pour ce faire, remplacez `animation.mp4` par un autre nom.

## Crédits et licence

Ce document est une adaptation du tutoriel officiel [Push Button Stop Motion](https://projects.raspberrypi.org/en/projects/push-button-stop-motion/) de la [fondation Raspberry Pi](https://www.raspberrypi.org/) disponible sous licence Creative Commons [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.fr).
